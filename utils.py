from typing import List

import pandas as pd

from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.express as px


def plot_variable(
    df: pd.DataFrame, variable_name: str, unit: str = "", title: str = ""
):
    fig = px.line(
        df,
        x="time",
        y=variable_name,
        height=350,
        width=800,
    )
    fig["layout"]["yaxis"]["title"] = unit
    fig.update_layout(title_text=title, title_x=0.5, title_y=0.95)
    fig.show()


def plot_comparison(
    pollutant: str,
    sat_df: pd.DataFrame,
    icos_df: pd.DataFrame,
    sat_unit: str = "",
    icos_unit: str = "",
    title: str = "",
):
    """ """
    fig = make_subplots(
        rows=2,
        cols=1,
        shared_xaxes=True,
        shared_yaxes=True,
        x_title="Time",
        vertical_spacing=0.1,
    )

    fig.append_trace(
        go.Scatter(
            x=icos_df["time"],
            y=icos_df[f"icos_{pollutant}"],
            mode="markers",
            marker=dict(size=3),
            opacity=0.9,
            name=f"ICOS {pollutant.upper()}",
        ),
        row=1,
        col=1,
    )

    fig.append_trace(
        go.Scatter(
            x=sat_df["time"],
            y=sat_df[f"sat_{pollutant}"],
            mode="markers",
            marker=dict(size=3),
            opacity=0.9,
            name=f"satellite {pollutant.upper()}",
        ),
        row=2,
        col=1,
    )

    fig.update_layout(
        height=500,
        width=800,
        title_text=title,
        title_y=0.95,
        legend_title="Data Source",
    )

    fig["layout"]["yaxis"]["title"] = f"{pollutant.upper()} ({icos_unit})"
    fig["layout"]["yaxis2"]["title"] = sat_unit

    fig.show()


def plot_stations_variables(
    stations: List[str],
    variables: List[List[float]],
    variable_names=List[str],
    width=800,
    height=500,
):
    fig = make_subplots(
        rows=1,
        cols=len(variables),
        shared_xaxes=True,
        shared_yaxes=False,
        vertical_spacing=0.1,
        horizontal_spacing=0.1,
    )

    for var_id, (var, var_name) in enumerate(zip(variables, variable_names)):
        fig.append_trace(
            go.Scatter(
                x=stations,
                y=var,
                mode="markers",
                name=f"{var_name}",
            ),
            row=1,
            col=var_id + 1,
        )

    title_vars = " and ".join(variable_names)
    fig.update_layout(
        height=height,
        width=width,
        title_text=f"{title_vars} in stations of interest",
        title_x=0.45,
        yaxis_title="",
        legend_title="Variables",
    )
    fig.update_xaxes(tickangle=45)
    for var_id, var_name in enumerate(variable_names):
        if var_id != 0:
            fig["layout"][f"yaxis{var_id+1}"]["title"] = f"{var_name}"
        else:
            fig["layout"]["yaxis"]["title"] = f"{var_name}"

    fig.show()
