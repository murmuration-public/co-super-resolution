# ✨ CO super-resolution

Generating high resolution CO maps by combining different satellite data.

<img src="images/low_vs_high_germany.png" width=700px>

Notebooks :
* `Correlation_ICOS_CO` : analyses the correlations between satellite data and ICOS in-situ data
* `super_resolution_model` : generates a training/testing dataframe and trains a model.
* `High Resolution Map Creation` : creates a dataset with all data on one date and predicts on it.
* `CO super resolution` : demonstrates the model from dataframe to analysis.

In the `data_gathering` directory, you will find all the notebooks needed to preprocess each data described below:

| Source  | Variables | Temporal Resolution | Spatial Resolution |
| ------------- |:-------------:|:-:|:-:|
| [CAMS air quality forecasts](https://ads.atmosphere.copernicus.eu/cdsapp#!/dataset/cams-europe-air-quality-forecasts?tab=overview) | CO | hourly | 0.1° (~9km) |
| [ERA5-Land hourly data](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-land?tab=form) | wind, precipitation, temperature | hourly | 0.1° (~9km) |
| [Vito Land Cover](https://lcviewer.vito.be/2019) | land cover | - | 100m |
| [NOAA Topography](https://www.ncei.noaa.gov/maps/grid-extract/) | topography | - | 30m |
| [Worldpop](https://data.humdata.org/dataset/worldpop-population-density-for-germany) | population density | - | 1km |
| [ICOS](https://data.icos-cp.eu/portal/#%7B%22filterCategories%22%3A%7B%22project%22%3A%5B%22icos%22%5D%2C%22level%22%3A%5B1%2C2%5D%2C%22stationclass%22%3A%5B%22ICOS%22%5D%2C%22type%22%3A%5B%22atcCoL2DataObject%22%5D%7D%7D) | CO | hourly | in-situ |

<img src="images/commission_europeenne.png" width=700px>