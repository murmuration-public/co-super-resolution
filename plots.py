import numpy as np
import shap
import matplotlib.pyplot as plt
from typing import Dict, List
import os

import plotly.express as px
from plotly.subplots import make_subplots

from bokeh.plotting import figure, output_file, show
from bokeh.palettes import Bokeh, Viridis
from bokeh.models.widgets import Panel, Tabs, Div, Paragraph, PreText
from bokeh.models import BoxAnnotation, Toggle
from bokeh.layouts import row, Column, gridplot

from sklearn.metrics import mean_absolute_error, mean_squared_error
from sklearn.metrics import r2_score


def plot_model_evaluations(
    model,
    predictions: np.array,
    ground_truth: np.array,
    X_test,
    model_name: str,
    params: Dict,
    sat_label="P_NO2",
    output_dir="../plots",
    instance_idx=0,
):
    input_features_str = "_".join(params["input_features"])
    params_str = input_features_str + "_" + str(params["test_size"])
    output_file(os.path.join(output_dir, f"{model_name}_{params_str}.html"))

    pre = Div(
        text=f"""<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Hello, world!</title>
    </head>
    <h1 style="width: max-content;">{model_name}</h1>"""
    )

    mae = mean_absolute_error(ground_truth, predictions)
    mse = mean_squared_error(ground_truth, predictions)
    r2 = r2_score(ground_truth, predictions)

    info = Div(
        text=f"""
    <p><b>Params : </b>{params}</p>
    <b>Ground truth : </b> <ul><li>Min : {ground_truth.min()}</li> <li>Max : {ground_truth.max()}</li> <li>Mean : {ground_truth.mean()}</li></ul>
    <b>Predictions : </b> <ul><li>Min : {predictions.min()}</li> <li>Max : {predictions.max()}</li> <li>Mean : {predictions.mean()}</li></ul>
    <p><b>Model evaluation</b></p>
    <ul><li>MAE: {mae}</li><li>MSE: {mse}</li><li>R2: {r2}</li></ul>
    """
    )

    # eval = Div(
    #     text=f"""
    #     <p><b>Model evaluation</b></p>
    #     <ul><li>MAE: {mae}</li><li>R2: {r2}</li></ul>
    #     """
    # )

    tabs = []
    figures = [
        "Success",
        "Comparison ground truth / predictions",
        "Differences with ground truth",
    ]
    shap_figs = ["Force plot"]

    sat_diffs, pred_diffs = compute_differences(
        X_test, predictions, ground_truth, sat_label
    )

    for fig_id, fig_name in enumerate(figures):

        if fig_name == "Comparison ground truth / predictions":
            fig = plot_comparison_gt_pred(
                fig_name, ground_truth, predictions, X_test, sat_label
            )

        if fig_name == "Differences with ground truth":
            fig = plot_differences_with_gt(fig_name, sat_diffs, pred_diffs)

        if fig_name == "Success":
            nb_true_pred = []
            nb_true_sat = []
            for th in range(40):
                count = len(
                    [
                        1
                        for i, x in enumerate(ground_truth)
                        if abs(x - predictions[i]) < th
                    ]
                )
                nb_true_pred.append(count / len(predictions))
                count = len(
                    [
                        1
                        for i, x in enumerate(ground_truth)
                        if abs(x - X_test[sat_label].values[i]) < th
                    ]
                )
                nb_true_sat.append(count / len(predictions))
            fig = plot_success(fig_name, nb_true_pred, nb_true_sat)

        tab = Panel(child=fig, title=f"{fig_name}")
        tabs.append(tab)

    tabs = Tabs(tabs=tabs)

    # SHAP

    # Compute shap values
    explainer = shap.TreeExplainer(model)
    shap_values = explainer.shap_values(X_test)

    shap_summary_path = (
        f"{output_dir}/images/shap_summary_{model_name}_{params_str}.png"
    )
    shap_force_path = (
        f"{output_dir}/images/shap_force_instance_{model_name}_{params_str}.png"
    )
    print("PATH exists : ", os.path.exists(shap_summary_path))
    save_shap_graphs(
        explainer,
        shap_values,
        X_test,
        shap_summary_path,
        shap_force_path,
        idx=instance_idx,
    )
    shap_plots = Div(
        text=f"""<h3 style="width: max-content;">Shap Analysis</h3>
                <img src="../{shap_summary_path}" width="600" alt="shap summary plot">
                <img src="../{shap_force_path}" width="1000" alt="shap force plot">"""
    )

    # shap_plots = Div(
    #     text=f"""<h3 style="width: max-content;">Shap Analysis</h3>
    #             <img src="../plots/images/shap_summary_{model_name}_{params_str}.png" width="600" alt="image won't load">
    #             <img src="../plots/images/shap_force_instance_{model_name}_{params_str}.png" width="1000" alt="image won't load">"""
    # )

    # RENDER
    show(
        Column(
            pre,
            info,
            tabs,
            shap_plots,
        )
    )

    return nb_true_pred


def compute_differences(X_test, pred, gt, sat_label="P_NO2"):
    sat_diffs = []
    pred_diffs = []
    sat = X_test[sat_label].values

    for y_p, y_t, x_s in zip(pred, gt, sat):
        pred_diff = abs(y_p - y_t)
        sat_diff = abs(x_s - y_t)
        sat_diffs.append(sat_diff)
        pred_diffs.append(pred_diff)

    return sat_diffs, pred_diffs


def predict_on_image(model, datacube, input_features, idx=0):
    source = datacube.isel(time=idx)
    source_df = source.to_dataframe()
    source_df_input = source_df.reset_index()[input_features]

    output = model.predict(source_df_input)
    source_df["output"] = output
    output_xarray = source_df.to_xarray()

    return output_xarray


def plot_output_details(
    output_xarray,
    datacube,
    idx=0,
    step=100,
    input_features: List[str] = [],
    sat_label="P_NO2",
    target_label="AirQuality",
):
    plots = []

    for lat in range(0, 500, step):
        if (lat / step) % 2 == 0:
            for lon in range(0, 700, step):
                min_x, max_x, min_y, max_y = (lat, lat + step, lon, lon + step)

                im1 = figure(title="Low resolution", height=200, width=200)
                im1.image(
                    image=[output_xarray[sat_label][min_x:max_x, min_y:max_y].values],
                    palette=Viridis[10],
                    x=0,
                    y=0,
                    dw=10,
                    dh=10,
                    level="image",
                )
                im2 = figure(title="High resolution", height=200, width=200)
                im2.image(
                    image=[output_xarray["output"][min_x:max_x, min_y:max_y].values],
                    palette=Viridis[10],
                    x=0,
                    y=0,
                    dw=10,
                    dh=10,
                    level="image",
                )

                images = [im1, im2]

                for feature in input_features:
                    im = figure(title=f"{feature}", height=200, width=200)
                    im.image(
                        image=[
                            output_xarray[f"{feature}"][min_x:max_x, min_y:max_y].values
                        ],
                        palette=Viridis[10],
                        x=0,
                        y=0,
                        dw=10,
                        dh=10,
                        level="image",
                    )
                    im.grid.grid_line_width = 0
                    images.append(im)
                im1.grid.grid_line_width = 0
                im2.grid.grid_line_width = 0

                idx += 1
                plots.append(images)
    return plots


def save_shap_graphs(explainer, shap_values, X_test, summary_path, force_path, idx=6):
    shap.summary_plot(shap_values, X_test, show=False)
    plt.savefig(
        summary_path,
        format="png",
        dpi=150,
        bbox_inches="tight",
    )

    shap.force_plot(
        explainer.expected_value,
        shap_values[idx],
        X_test.iloc[idx, :],
        show=False,
        matplotlib=True,
    ).savefig(
        force_path,
        format="png",
        dpi=150,
        bbox_inches="tight",
    )


def plot_comparison_gt_pred(fig_name, ground_truth, predictions, X_test, sat_label):
    fig = figure(
        plot_width=1200,
        plot_height=400,
        title=f"{fig_name}",
        x_axis_label="time and place",
        y_axis_label="NO2",
    )
    fig.line(
        range(len(ground_truth)),
        ground_truth,
        legend_label="ground truth",
        line_width=2,
        color="blue",
        alpha=0.6,
    )
    fig.line(
        range(len(predictions)),
        predictions,
        legend_label="predictions",
        line_width=2,
        color="red",
        alpha=0.6,
    )
    fig.line(
        range(len(X_test[sat_label].values)),
        X_test[sat_label].values,
        legend_label="satellite",
        line_width=2,
        color="purple",
        alpha=0.6,
    )
    fig.legend.click_policy = "hide"

    return fig


def plot_differences_with_gt(fig_name, sat_diffs, pred_diffs):
    fig = figure(
        plot_width=1200,
        plot_height=400,
        title=f"{fig_name}",
        x_axis_label="time and place",
        y_axis_label="abs NO2 diff",
    )
    fig.line(
        x=range(len(sat_diffs)),
        y=sat_diffs,
        legend_label="satellite",
        line_width=2,
        color="green",
        alpha=0.6,
    )
    fig.line(
        range(len(pred_diffs)),
        pred_diffs,
        legend_label="predictions",
        line_width=2,
        color="red",
        alpha=0.6,
    )
    return fig


def plot_success(fig_name, nb_true_pred, nb_true_sat):
    fig = figure(
        plot_width=1000,
        plot_height=400,
        title=f"{fig_name}",
        x_axis_label="threshold",
        y_axis_label="percentage of success",
    )
    fig.line(
        range(len(nb_true_pred)),
        nb_true_pred,
        line_width=2,
        color=Bokeh[3][0],
        legend_label="prediction",
    )
    fig.line(
        range(len(nb_true_sat)),
        nb_true_sat,
        line_width=2,
        color=Bokeh[3][1],
        legend_label="satellite",
    )
    fig.legend.location = "bottom_right"
    fig.legend.click_policy = "hide"
    # plt.plot(nb_true)
    return fig


def plot_prediction(xgb_image, date_str=None, cmin=None, cmax=None):
    fig = make_subplots(
        rows=1,
        cols=2,
        shared_xaxes=True,
        shared_yaxes=True,
        x_title="longitude",
        y_title="latitude",
        horizontal_spacing=0.05,
    )

    fig.append_trace(
        px.imshow(xgb_image["CAMS_no2"].squeeze(), zmin=0, zmax=100).data[0],
        row=1,
        col=1,
    )

    fig.append_trace(
        px.imshow(xgb_image["output"].squeeze(), zmin=0, zmax=100).data[0], row=1, col=2
    )

    if date_str is not None:
        title = f"NO2 in Ile de France on {date_str}"
    else:
        title = "NO2 in Ile de France"

    fig.update_layout(
        height=500,
        width=900,
        title_text=title,
        title_x=0.5,
        title_y=0.88,
        coloraxis_cmin=cmin,
        coloraxis_cmax=cmax,
        coloraxis_colorbar=dict(
            #         title=r'$ NO_2 (\mu g)$',
            title="NO2 (µg/m3)"
        ),
    )
    fig["layout"]["scene"]["zaxis"].update(range=[0, 100])

    fig.show()
